import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule , TranslateService} from '@ngx-translate/core';
import {MessagesRouting} from './messages.routing';
import { MessagesSharedModule } from './messages.shared';
import { FormsModule } from '@angular/forms';
import {environment} from '../../environments/environment';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { SendMessageToClassComponent } from './components/send-message-to-class/send-message-to-class.component';


@NgModule({
  imports: [
    CommonModule,
    MessagesRouting,
    TranslateModule,
    MessagesSharedModule,
    FormsModule,
    InfiniteScrollModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    SendMessageToClassComponent
  ],
  declarations: [MessagesHomeComponent, MessagesListComponent, SendMessageToClassComponent]
})
export class MessagesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/messages.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
