import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-courses-grades-step-1',
  templateUrl: './courses-grades-step-1.component.html',
})
export class CoursesGradesStep1Component implements OnInit {

  @ViewChild('fileinput') fileinput;
  @Output() selectedFileOutput = new EventEmitter();
  @Input() errorMessage = '';
  @Input() isLoading = false;

  public selectedFile: File;

  constructor(private fb: FormBuilder,
              private cd: ChangeDetectorRef) { }

  ngOnInit() {

  }

  onFileChanged(event) {
    this.selectedFile = this.fileinput.nativeElement.files[0];
    this.selectedFileOutput.emit(this.selectedFile);
  }

  reset() {
    this.fileinput.nativeElement.value = '';
  }
}
