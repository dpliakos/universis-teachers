import { Component, OnInit } from '@angular/core';
import {ThesesService} from '../../services/theses.service';
import {LoadingService} from '@universis/common';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-theses-currents',
  templateUrl: './theses-currents.component.html',
  styleUrls: ['../theses-completed/theses-completed.component.scss']
})
export class ThesesCurrentsComponent implements OnInit {
  public currentTheses: any = [];
  public isLoading = true;   // Only if data is loaded

  constructor( private thesesServices: ThesesService,
               private _loadingService: LoadingService,
               private _errorService: ErrorService
  ) { }

  ngOnInit() {
    this._loadingService.showLoading();
    this.thesesServices.getCurrentTheses().then((res) => {
      this.currentTheses = res;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }).catch(err => {
      // hide loading
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

}
