import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {ThesesCompletedComponent} from './components/theses-completed/theses-completed.component';
import {ThesesCurrentsComponent} from './components/theses-currents/theses-currents.component';

const routes: Routes = [
  {
    path: '',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  },
  {
    path: 'current',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'completed',
    component: ThesesCompletedComponent,
    canActivate: [
      AuthGuard
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThesesRoutingModule { }
